const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const Usuario = require('./app/models/usuario');
const CupomDesconto = require('./app/models/cupomdesconto');

// const urlBanco = 'mongodb+srv://admin:senhabanco@cluster0-p4f4k.mongodb.net/ecommerce?retryWrites=true&w=majority';

const porta = 3000;
mongoose.connect("mongodb+srv://admin:senhabanco@cluster0-p4f4k.mongodb.net/ecommerce?retryWrites=true&w=majority", {useNewUrlParser:true});

app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

app.use((req, res, next) => {
  console.log(`algo esta acontecendo`, req.url);
  next();
})

app.get('/', (req, res) => {
  res.send('bem vindo a loja virtual');
});

app.get('/usuarios', (req, res) => {
  //get usuarios cadastrados no mongoDB
  Usuario.find((erro, usuarios) => {
    if(erro){
      console.log(erro)
      res.send('Erro ao recuperar usuários', erro);
    }
    else{
      res.json(usuarios);
    } 
  });
});

app.post('/cupons', (req, res) => {
  let cupomDesconto = new CupomDesconto();

  cupomDesconto.dataInicial = req.body.dataInicial;
  cupomDesconto.dataFinal = req.body.dataFinal;
  cupomDesconto.valorInicial = req.body.valorInicial;
  cupomDesconto.valorFinal = req.body.valorFinal;
  cupomDesconto.quantidadeCupons = req.body.quantidadeCupons;
  cupomDesconto.quantidadeUsada = req.body.quantidadeUsada;
  cupomDesconto.percentualDesconto = req.body.percentualDesconto;
  
  console.log(cupomDesconto)

  cupomDesconto.save(erro => {
    if(erro){
      console.log(erro)
      res.status(500).send('erro ao gravar cupom' + erro);
    }
    res.json({message: "cupom salvo com sucesso"});
  })

});

app.get('/cupons', (req, res) => {
  //get usuarios cadastrados no mongoDB
  CupomDesconto.find((erro, cupons) => {
    if(erro){
      res.send('Erro ao recuperar usuários', erro);
    }
    else{
      res.json(cupons);
    } 
  });
});

app.listen(porta, (req, res) => {
	console.log(`servidor inicializado na porta ${porta}`);
})